import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    """
    # Use the Pexels API

    # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": "HEBObOHZIJkIwLF2g1jAN5Emy64LIQAmev11Rd7rUN8ODUFhpjFPB5Rs",
    #   "Content-Type": "application/json",
        }
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    resp = requests.get(
        f'https://api.pexels.com/v1/search?query={city},{state}&per_page=1',
        headers=headers
    )
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    return resp.json()['photos'][0]["url"]
    """
    """
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + str(state)}
    url = f"https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)
    try:
        return {"picture_url": content}
    except(KeyError, IndexError):
        return {"picture_url": None}
    """
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(
        f"https://api.pexels.com/v1/search?query={city},{state}",
        headers=headers,
    )
    return (response.json()["photos"][0]["src"]["original"])


def get_weather_data(city, state):
    """
    # Use the Open Weather API

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    """
    """
    params = {"q": city + "," + state + "," + "US", "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    print("Geo Location", content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except KeyError:
        return {
            "lat": None,
            "lon": None,
        }

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY
    }

    url = "http://api.openweathermap.org/geo/2.5/direct"

    weather_response = requests.get(url, params=params)
    weather_content = json.loads(weather_response.content)

    try:
        weather_dict = {
            "main_temperature": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"]
        }
        return weather_dict
    except KeyError:
        return None
    """
    """
    # First API call to get geo location
    geo_params = {"q": city + "," + state + "," + "US", "appid": OPEN_WEATHER_API_KEY}
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"

    geo_response = requests.get(geo_url, params=geo_params)
    geo_content = geo_response.json()  # Use .json() to directly parse JSON

    print("Geo Location", geo_content)

    # Check if the HTTP request was successful
    if not geo_response.ok:
        return {"lat": None, "lon": None}

    # Extract latitude and longitude
    try:
        latitude = geo_content[0]["lat"]
        longitude = geo_content[0]["lon"]
    except (KeyError, IndexError):
        return {"lat": None, "lon": None}

    # Second API call to get weather data
    weather_params = {"lat": latitude, "lon": longitude, "appid": OPEN_WEATHER_API_KEY}
    weather_url = "http://api.openweathermap.org/geo/2.5/direct"

    weather_response = requests.get(weather_url, params=weather_params)
    weather_content = weather_response.json()

    # Check if the HTTP request was successful
    if not weather_response.ok:
        return None

    # Extract weather information
    try:
        weather_dict = {
            "main_temperature": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"]
        }
        return weather_dict
    except (KeyError, IndexError):
        return None
    """

    # Use the Open Weather API

    # Step 1: Geocoding API
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {"q": f"{city},{state},US", "appid": OPEN_WEATHER_API_KEY}

    # Make the geocoding API request
    geo_response = requests.get(geo_url, params=geo_params)

    # Check if the geocoding request was successful
    if not geo_response.ok:
        return {"lat": None, "lon": None}

    # Parse the JSON response for geocoding
    geo_content = geo_response.json()

    # Get the latitude and longitude from the response
    try:
        latitude = geo_content[0]["lat"]
        longitude = geo_content[0]["lon"]
    except (KeyError, IndexError):
        return {"lat": None, "lon": None}

    # Step 2: Current Weather API
    weather_url = "http://api.openweathermap.org/geo/2.5/direct"
    weather_params = {"lat": latitude, "lon": longitude, "appid": OPEN_WEATHER_API_KEY}

    # Make the current weather API request
    weather_response = requests.get(weather_url, params=weather_params)

    # Check if the current weather request was successful
    if not weather_response.ok:
        return None

    # Parse the JSON response for current weather
    weather_content = weather_response.json()

    # Get the main temperature and the weather's description and put them in a dictionary
    try:
        weather_dict = {
            "main_temperature": weather_content["main"]["temp"],
            "description": weather_content["weather"][0]["description"]
        }
        return weather_dict
    except (KeyError, IndexError):
        return None
